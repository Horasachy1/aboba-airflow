import datetime
import zipfile

from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.docker.operators.docker import DockerOperator
from airflow.providers.google.cloud.transfers.local_to_gcs import (
    LocalFilesystemToGCSOperator,
)
from airflow.providers.discord.operators.discord_webhook import DiscordWebhookOperator
from airflow.operators.python import PythonOperator
from docker.types import Mount

start_date = datetime.datetime(2015, 1, 1)
default_args = {"owner": "deals2b"}


def _get_files_path(**kwargs):
    ti = kwargs["ti"]
    path = "/crawler/crawler-data"
    ti.xcom_push("files_path", value=path)
    return path


def _create_zip_file(**kwargs):
    from shutil import make_archive
    ti = kwargs["ti"]
    files_path = ti.xcom_pull(key="files_path")
    make_archive(f"{files_path}", "zip", root_dir=f"{files_path}")


with DAG(
        dag_id="aboba_dag",
        schedule_interval=None,
        tags=["aboba", "scrapy"],
        default_args=default_args,
        start_date=start_date,
) as dag:
    start_aboba = DummyOperator(
        task_id="start_aboba",
        do_xcom_push=False,
    )
    aboba_start_info = DiscordWebhookOperator(
        http_conn_id="discord",
        webhook_endpoint="webhooks/983086451338121236/wf22gng0y1VkLYMy4N3XdHP2o51fp5mb9bn6pxreKF2mJj2Af3OPuU-CH8rlcEfdIIEl",
        username="Aboba Airflow",
        message='Aboba start process `{{dag_run.conf["target"]}}`',
        task_id="aboba_start_info",
        avatar_url="https://cdn.dribbble.com/users/371094/screenshots/3362510/baboon.jpg?compress=1&resize=400x300",
        tts=False,
        do_xcom_push=False,
    )
    get_files_path = PythonOperator(
        provide_context=True,
        task_id="get_files_path",
        python_callable=_get_files_path,
        do_xcom_push=True,
    )
    parse_pdf_data = DockerOperator(
        task_id="parse_pdf_data",
        api_version="1.41",
        auto_remove=True,
        force_pull=True,
        xcom_all=True,
        docker_url="tcp://docker-proxy:2375",
        image="registry.gitlab.com/horasachy1/aboba-crawl:main",
        docker_conn_id="gitlab",
        network_mode="bridge",
        privileged=True,
        do_xcom_push=True,
        command="/bin/bash -c 'cd /crawler/aboba-crawl && ls && \
            scrapy crawl pdf -a urls=\"{{ dag_run.conf["'"target"]}}"\'',
        entrypoint=[],
        # mounts=[
        #     Mount(target="/crawler/aboba-crawl/crawler-data", source="aboba-airflow_airflow-crawler"),
        #     Mount(target="/crawler-data", source="aboba-airflow_airflow-crawler"),
        #     Mount(target="/tmp", source="aboba-airflow_airflow-crawler"),
        #     Mount(target="/tmp/crawl-data", source="aboba-airflow_crawl-data")
        # ],
    )

    create_zip_file = PythonOperator(
        provide_context=True,
        task_id="create_zip_file",
        python_callable=_create_zip_file,
        do_xcom_push=False,
    )

    upload_pdf_results_google_cloud_file = LocalFilesystemToGCSOperator(
        task_id="upload_pdf_results_google_cloud_file",
        src="/crawler/crawler-data.zip",
        dst="aboba/",
        bucket="aboba_bucket",
        gcp_conn_id="google",
        do_xcom_push=False,
        gzip=True,
    )
    aboba_finish_process = DiscordWebhookOperator(
        http_conn_id="discord",
        webhook_endpoint="webhooks/983086451338121236/wf22gng0y1VkLYMy4N3XdHP2o51fp5mb9",
        username="Aboba Airflow",
        message='Aboba finish process `{{dag_run.conf["target"]}}`',
        task_id="aboba_finish_process",
        avatar_url="https://cdn.dribbble.com/users/371094/screenshots/3362510/baboon.jpg?compress=1&resize=400x300",
        tts=False,
        do_xcom_push=False,
    )
    finish_aboba = DummyOperator(
        task_id="finish_aboba",
        do_xcom_push=False,
    )

    start_aboba >> [aboba_start_info, get_files_path] >> parse_pdf_data
    parse_pdf_data >> create_zip_file >> upload_pdf_results_google_cloud_file
    upload_pdf_results_google_cloud_file >> aboba_finish_process >> finish_aboba
