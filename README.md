# Aboba-airflow

This project is based on the ETL (Extract, Transform, Load) business model implemented with the [Apache Airflow][airflow] tool.

## Basic concepts

The /dags directory contains the main logic.
One Dag includes a chain of sequential operations, each of which has its own task.

- DAG (Directed Acyclic Graph) - is the core concept of Airflow, collecting Tasks together, organized with dependencies and relationships to say how they should run.
- Operator - is conceptually a template for a predefined Task, that you can just define declaratively inside your DAG.
- Task - is the basic unit of execution in Airflow. Tasks are arranged into DAGs, and then have upstream and downstream dependencies set between them into order to express the order they should run in.
In /dags 

## Installation

### linux:
if not exist dirs:
```shell
mkdir ./dags ./logs ./plugins 
````
if empty or not exist .env file:
```shell
echo -e 'AIRFLOW_UID=0\nAIRFLOW_GID=0\n_PIP_ADDITIONAL_REQUIREMENTS="apache-airflow-providers-http apache-airflow-providers-discord langdetect~=1.0.9 spacy~=3.1.1 tqdm~=4.61.1"' > .env
```
```shell
docker-compose up airflow-init
docker login registry.gitlab.com
docker-compose build
```
### windows:
if not exist dirs:
```shell
mkdir dags   
mkdir logs
mkdir plugins
````
if empty or not exist .env file:
```shell
echo 'AIRFLOW_UID=0\nAIRFLOW_GID=0\n_PIP_ADDITIONAL_REQUIREMENTS="apache-airflow-providers-http apache-airflow-providers-discord langdetect~=1.0.9 spacy~=3.1.1 tqdm~=4.61.1"' > .env
```
```shell
docker-compose up airflow-init
docker login registry.gitlab.com
docker-compose build
````
## Running servers

```shell
docker-compose up
```
## Airflow server: 
```
http://127.0.0.1:8080/
```
## Flower server: 
```
http://127.0.0.1:5555/dashboard
```

## Usage
Go to: 
```
http://localhost:8080
```
Login: 
```
username: airflow
password: airflow
```
![airflow-dashboard](docs/img/login.jpg)

### Create a new connection to run docker and discord operations.

go to http://localhost:8080/connection/add or click to Admin->Connections:

Docker:
- Conn Id - "gitlab"
- Conn type - "Docker"
- Registry URL - "registry.gitlab.com"
- enter gitlab username and password
- push save

Discord:
- Conn Id - "discord"
- Conn type - "Discord"
- Host - "https://discord.com/api/"
- push save

Google Cloud:
- Conn Id - "google"
- Conn type - "Google Cloud"
- Keyfile JSON - "<copy the contents of the file secrets/aboba.json>"
- push save

### Running dag with parameters:

![airflow-dashboard](docs/img/trigger-dag-with-params.png)

![airflow-dashboard](docs/img/trigger-dag.png)
push Trigger

### Running dags without parameters:

![airflow-dashboard](docs/img/run-dag.jpg)


[airflow]: <https://airflow.apache.org/docs/>

